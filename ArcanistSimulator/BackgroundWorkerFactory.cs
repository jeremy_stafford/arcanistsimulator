﻿using System.ComponentModel;

namespace ArcanistSimulator
{
    public static class BackgroundWorkerFactory
    {
        public static BackgroundWorker GetBackgroundWorker(DoWorkEventHandler doWork, ProgressChangedEventHandler onProgress, RunWorkerCompletedEventHandler onComplete)
        {
            var bw = new BackgroundWorker();
            bw.DoWork += doWork;
            bw.ProgressChanged += onProgress;
            bw.RunWorkerCompleted += onComplete;

            bw.WorkerReportsProgress = true;
            return bw;
        }
    }
}