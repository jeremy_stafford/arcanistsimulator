﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArcanistSimulator
{
    public class Spell : SpellBase
    {
        public override async Task<bool> CastSpellAsync(Target target)
        {
            throw new NotImplementedException();
        }

        public override int CalculateDamage()
        {
            return Potency;
        }
    }
}