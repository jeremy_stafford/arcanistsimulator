﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Timer = System.Windows.Forms.Timer;

namespace ArcanistSimulator
{
    public abstract class SpellBase
    {
        public int Potency { get; set; }
        public int CastTime { get; set; }
        public string SpellName { get; set; }
        public bool CausesGlobalCooldown { get; set; }
        public CancellationTokenSource CancellationTokenSource { get; set; }

        public abstract Task<bool> CastSpellAsync(Target target);
        public abstract int CalculateDamage();

        public virtual async Task<Target> RunDamage(Target target, int tickSpeed)
        {
            var dealDamage = CalculateDamage();
            while (!CancellationTokenSource.Token.IsCancellationRequested)
            {
                Thread.Sleep(tickSpeed);
                target.ReceiveDamage(dealDamage, SpellName);
            }
            return target;
        }

        protected virtual async Task<bool> Run(Target target)
        {
            if (this is Dot)
            {
                var dot = (Dot)this;
                await RunDamage(target, dot.TickSpeed);
                OnSpellComplete(EventArgs.Empty);
            }
            else
            {
                target.ReceiveDamage(CalculateDamage(), SpellName);
            }
            return true;
        }

        protected void OnSpellComplete(EventArgs e)
        {
            if (SpellComplete != null)
            {
                SpellComplete(this, e);
            }
        }

        public event CastTimeUpdateHandler CastTimeUpdate;
        public event GlobalCooldownUpdateHandler GlobalCooldownUpdate;
        public event SpellCompletedHandler SpellComplete;

        #region -- Timers --
        protected async Task<bool> RunCastTimer()
        {
            var timer = new Timer();
            timer.Tick += CastTimer_Tick;

            timer.Start();

            int elapsed = 0;
            do
            {
                elapsed++;
                Thread.Sleep(1);

            } while (elapsed <= CastTime);

            return true;
        }

        protected async Task<bool> RunGcdTimer()
        {
            var timer = new Timer();
            timer.Tick += CastTimer_Tick;

            var startTime = DateTime.Now;
            timer.Start();

            int elapsed = 0;

            const int globalCoolDown = 2500; // get this from somewhere else
            if (!CausesGlobalCooldown) return true;

            timer.Tick += GlobalCooldown_Tick;

            elapsed = 0;
            timer.Start();
            do
            {
                elapsed++;
                Thread.Sleep(1);
            } while (elapsed <= globalCoolDown);

            return true;
        }

        public void CastTimer_Tick(object sender, EventArgs e)
        {
            OnCastTimerTick(e);
        }

        public void GlobalCooldown_Tick(object sender, EventArgs e)
        {
            OnGlobalCooldown_Tick(e);
        }

        private void OnCastTimerTick(EventArgs e)
        {
            if (CastTimeUpdate != null)
            {
                CastTimeUpdate(this, e);
            }
        }

        private void OnGlobalCooldown_Tick(EventArgs e)
        {
            if (GlobalCooldownUpdate != null)
            {
                GlobalCooldownUpdate(this, e);
            }
        }
        #endregion
    }

    public delegate int CastTimeUpdateHandler(object sender, EventArgs e);
    public delegate int GlobalCooldownUpdateHandler(object sender, EventArgs e);
    public delegate void SpellCompletedHandler(object sender, EventArgs e);
}