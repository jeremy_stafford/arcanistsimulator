﻿using System.Collections.Generic;

namespace ArcanistSimulator
{
    public class SpellBook : Dictionary<SpellName, Spell>
    {
        public SpellBook()
        {
            Add(SpellName.Bio, new Dot { CastTime = 0, Duration = 18000, InitialDamage = 0, TickSpeed = 3000, Potency = 40, SpellName = "Bio", CausesGlobalCooldown = true });
            Add(SpellName.Bio2, new Dot { CastTime = 2500, Duration = 30000, InitialDamage = 0, TickSpeed = 3000, Potency = 80, SpellName = "Bio II", CausesGlobalCooldown = false });
            Add(SpellName.Miasma, new Dot { CastTime = 2500, Duration = 24000, Potency = 20, TickSpeed = 3000, SpellName = "Miasma", CausesGlobalCooldown = false });
            Add(SpellName.Aero, new Dot { CastTime = 0, Duration = 18000, InitialDamage = 50, TickSpeed = 3000, Potency = 20, SpellName = "Aero", CausesGlobalCooldown = true });
            Add(SpellName.Ruin, new Spell { CastTime = 25000, Potency = 80, SpellName = "Ruin", CausesGlobalCooldown = false });
        }
    }

    public enum SpellName
    {
        Bio,
        Bio2,
        Aero,
        Miasma,
        Ruin
    }
}
