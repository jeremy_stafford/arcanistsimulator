﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ArcanistSimulator
{
    public class Dot : Spell
    {
        public int InitialDamage { get; set; }
        public int Duration { get; set; }
        public int TickSpeed { get; set; }
        public bool IsActive { get; private set; }

        public override int CalculateDamage()
        {
            var damage = (Potency / (TickSpeed / 1000));
            return damage;
        }

        public override async Task<bool> CastSpellAsync(Target target)
        {
            // use cancellation token as timer
            CancellationTokenSource = new CancellationTokenSource();
            CancellationTokenSource.CancelAfter(TimeSpan.FromMilliseconds(Duration));

            await RunCastTimer();
            target.ReceiveDamage(InitialDamage, SpellName); //specific to some dots
            await RunGcdTimer();

            // run until duration elapsed
            Task.Factory.StartNew(() => Run(target));
            IsActive = true;

            return true;
        }

        protected override async Task<bool> Run(Target target)
        {
            IsActive = true;
            await base.Run(target);
            IsActive = false;

            return true;
        }
    }
}
