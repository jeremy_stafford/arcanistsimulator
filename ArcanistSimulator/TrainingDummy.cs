﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;

namespace ArcanistSimulator
{
    public class TrainingDummy : Target
    {
        public async Task<bool> TakeDamage(Spell spell)
        {
            if (spell is Dot)
            {
                var dot = (Dot) spell;
                return await dot.CastSpellAsync(this);
            }
            
            ReceiveDamage(spell.CalculateDamage(), spell.SpellName);
            return true;
        }
    }

    public delegate void DamageTakenHandler(object sender, DamageTakenEventArgs e);

    public class Target
    {
        public int MaxHealth { get; set; }
        public int DamageTaken { get; set; }

        public event DamageTakenHandler DamageTakenHandler;

        protected virtual void OnDamageTaken(DamageTakenEventArgs e)
        {
            if (DamageTakenHandler != null)
            {
                DamageTakenHandler(this, e);
            }
        }

        public void ReceiveDamage(int damage, string source)
        {
            DamageTaken += damage;
            OnDamageTaken(new DamageTakenEventArgs{ DamageDealt = damage, DamageSource = source});
        }
    }

    public class DamageTakenEventArgs : EventArgs
    {
        public int DamageDealt { get; set; }
        public string DamageSource { get; set; }
    }
}
