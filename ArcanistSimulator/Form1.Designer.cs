﻿namespace ArcanistSimulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.txtDuration = new System.Windows.Forms.TextBox();
            this.pbCastTimer = new System.Windows.Forms.ProgressBar();
            this.pbGlobalCooldown = new System.Windows.Forms.ProgressBar();
            this.lblCasting = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(12, 151);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(902, 255);
            this.txtOutput.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(839, 412);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtDuration
            // 
            this.txtDuration.Location = new System.Drawing.Point(814, 125);
            this.txtDuration.Name = "txtDuration";
            this.txtDuration.Size = new System.Drawing.Size(100, 20);
            this.txtDuration.TabIndex = 2;
            this.txtDuration.Text = "60";
            this.txtDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbCastTimer
            // 
            this.pbCastTimer.Location = new System.Drawing.Point(12, 411);
            this.pbCastTimer.Name = "pbCastTimer";
            this.pbCastTimer.Size = new System.Drawing.Size(296, 23);
            this.pbCastTimer.TabIndex = 3;
            // 
            // pbGlobalCooldown
            // 
            this.pbGlobalCooldown.Location = new System.Drawing.Point(314, 411);
            this.pbGlobalCooldown.Name = "pbGlobalCooldown";
            this.pbGlobalCooldown.Size = new System.Drawing.Size(88, 23);
            this.pbGlobalCooldown.TabIndex = 4;
            // 
            // lblCasting
            // 
            this.lblCasting.AutoSize = true;
            this.lblCasting.BackColor = System.Drawing.SystemColors.Control;
            this.lblCasting.Location = new System.Drawing.Point(408, 417);
            this.lblCasting.Name = "lblCasting";
            this.lblCasting.Size = new System.Drawing.Size(42, 13);
            this.lblCasting.TabIndex = 5;
            this.lblCasting.Text = "Casting";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 446);
            this.Controls.Add(this.lblCasting);
            this.Controls.Add(this.pbGlobalCooldown);
            this.Controls.Add(this.pbCastTimer);
            this.Controls.Add(this.txtDuration);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.txtOutput);
            this.Name = "Form1";
            this.Text = "Fight Simulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtDuration;
        private System.Windows.Forms.ProgressBar pbCastTimer;
        private System.Windows.Forms.ProgressBar pbGlobalCooldown;
        private System.Windows.Forms.Label lblCasting;
    }
}

