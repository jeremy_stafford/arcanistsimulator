﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace ArcanistSimulator
{
    public partial class Form1 : Form
    {
        private TrainingDummy _targetDummy;
        private SpellBook _spellBook;
        private BackgroundWorker _backgroundWorker;
        private int _fightDuration;
        private bool _isFightActive;
        private static bool _isCasting;

        public Form1()
        {
            InitializeComponent();
            
            _spellBook = new SpellBook();
            _targetDummy = new TrainingDummy();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (_isFightActive)
            {
                _isFightActive = false;
                btnStart.Text = "Start";
            }
            else
            {
                if (!int.TryParse(txtDuration.Text, out _fightDuration)) return;
                
                RunFightTimer();
                btnStart.Text = "Stop";
                StartRotation();
            }
        }

        private void StartRotation()
        {
            var attacks = new List<Spell>();
            attacks.Add(_spellBook[SpellName.Bio2]);
            attacks.Add(_spellBook[SpellName.Bio]);
            attacks.Add(_spellBook[SpellName.Miasma]);
            attacks.Add(_spellBook[SpellName.Aero]);

            foreach (var attack in attacks)
            {
                lblCasting.Text = string.Format("Casting {0}", attack.SpellName);
                var parameters = new AttackParameters
                {
                    Attack = attack,
                    Target = _targetDummy,
                    CastTimer = pbCastTimer,
                    GlobalTimer = pbGlobalCooldown
                };

                
                CastTimer_Do(parameters);
                lblCasting.Text = string.Empty;
            }
        }

        private void CastTimer_Do(AttackParameters parameters)
        {
            pbCastTimer.Value = 0;
            pbCastTimer.Maximum = parameters.Attack.CastTime;
            
            if (parameters.Attack.CastTime > 0)
            {
                do
                {
                    pbCastTimer.Value++;
                    Thread.Sleep(1);
                } while (pbCastTimer.Value < parameters.Attack.CastTime);
            }
            txtOutput.AppendText(string.Format("Casted {0}\r\n", parameters.Attack.SpellName));
            
            // execute gcd
            if (parameters.Attack.CausesGlobalCooldown)
            {
                RunGCD();
            }
        }

        private void RunGCD()
        {
            pbGlobalCooldown.Maximum = 2500;

            while (pbGlobalCooldown.Value < 2500)
            {
                pbGlobalCooldown.Value++;
                Thread.Sleep(1);
            };

            pbGlobalCooldown.Value = 0;
        }

        #region -- Fight Timer --
        private void RunFightTimer()
        {
            var bw = BackgroundWorkerFactory.GetBackgroundWorker(FightTimer_Do, FightTimer_Progress, FightTimer_Complete);
            bw.RunWorkerAsync(DateTime.Now);
        }

        private void FightTimer_Do(object sender, DoWorkEventArgs e)
        {
            var bw = sender as BackgroundWorker;
            e.Result = e.Argument;

            _isFightActive = true;
            do
            {
                bw.ReportProgress(1, e.Argument);
                Thread.Sleep(1000);
                _fightDuration--;
            } while (_fightDuration >= 0 && _isFightActive);

            _isFightActive = false;
        }

        private void FightTimer_Progress(object sender, ProgressChangedEventArgs e)
        {
            txtDuration.Text = _fightDuration.ToString();
        }

        private void FightTimer_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            txtOutput.AppendText("Fight Ended.\r\n");
        }
        #endregion

        
    }

    public class AttackParameters
    {
        public Target Target { get; set; }
        public Spell Attack { get; set; }
        public ProgressBar CastTimer { get; set; }
        public ProgressBar GlobalTimer { get; set; }
    }
}
