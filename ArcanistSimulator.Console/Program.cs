﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ArcanistSimulator.Console
{
    class Program
    {
        private static TrainingDummy _dummy;
        private static readonly SpellBook SpellBook = new SpellBook();

        static void Main(string[] args)
        {
            _dummy = new TrainingDummy();
            _dummy.DamageTakenHandler += SpellProgress;

            var attacks = new List<Spell>();
            attacks.Add(SpellBook[SpellName.Bio2]);
            attacks.Add(SpellBook[SpellName.Bio]);
            attacks.Add(SpellBook[SpellName.Miasma]);
            attacks.Add(SpellBook[SpellName.Aero]);

            RunAttacksAsync(attacks);

            // how do i know when all spells have finished?
            System.Console.WriteLine("Control Returned.");

            System.Console.ReadKey();
        }

        public static async void RunAttacksAsync(IEnumerable<Spell> attacks)
        {
            var allAttacks = attacks.ToList();
            foreach (var attack in allAttacks)
            {
                await RunCast(attack);
                System.Console.WriteLine("Casted {0}", attack.SpellName);
            }

            await WaitForAllCompleteAsync(allAttacks);
        }

        public static async Task WaitForAllCompleteAsync(List<Spell> attacks)
        {
            do
            {
                Thread.Sleep(1);
            } while (attacks.Where(a => a is Dot).Cast<Dot>().Any(a => a.IsActive));
        }

        public static async Task RunCast(Spell attack)
        {
            await attack.CastSpellAsync(_dummy);
        }

        private static void OnSpellCompleted(Spell spell)
        {
            System.Console.WriteLine("{0} finished", spell.SpellName);
        }

        private static void SpellProgress(object sender, DamageTakenEventArgs e)
        {
            System.Console.WriteLine("{0,10} {1,10} Total: {2,5}", e.DamageDealt, e.DamageSource, _dummy.DamageTaken);
        }

        public delegate Task<bool> ExecuteSpell(Spell spell);
        public delegate void OnCompleted(Spell spell);

        public class AttackAsync
        {
            public BackgroundWorker Attack { get; set; }
            public Spell Spell { get; set; }
        }
    }
}
